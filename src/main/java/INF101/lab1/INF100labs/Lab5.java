package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multiplyList = new ArrayList<Integer>();
        for(int i = 0; i < list.size(); i++) {
            multiplyList.add(list.get(i) * 2);
        }
        return multiplyList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> removed = new ArrayList<Integer>();
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) != 3) {
                removed.add(list.get(i));
            }
        }
        return removed;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> unique = new ArrayList<Integer>();
        for(int i = 0; i < list.size(); i++) {
            if(!unique.contains(list.get(i))) {
                unique.add(list.get(i));
            }
        }
        return unique;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for(int i = 0; i < b.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}