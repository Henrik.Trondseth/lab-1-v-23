package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static int sumOfRow(ArrayList<ArrayList<Integer>> grid, int row) {
        int sum = 0;
        ArrayList<Integer> midList = grid.get(row);
        for(int i = 0; i < midList.size(); i++) {
            sum += midList.get(i);
        }
        return sum;
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int numRows = grid.size();
        int numCols = grid.get(0).size();

        int[] rowSums = new int[numRows];
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                rowSums[i] += grid.get(i).get(j);
            }
        }

        for (int sum : rowSums) {
            if (sum != rowSums[0]) {
                return false;
            }
        }

        int[] colSums = new int[numCols];
        for (int j = 0; j < numCols; j++) {
            for (int i = 0; i < numRows; i++) {
                colSums[j] += grid.get(i).get(j);
            }
        }

        for (int sum : colSums) {
            if (sum != colSums[0]) {
                return false;
            }
        }

        return true;
    }

}